(function(window, undefined) {
'use strict';
var Math = window.Math,
    String = window.String,
    document = window.document,
    push = window.Array.prototype.push;
var BlockTable = window.BlockTable = function(rows, cols) {
    var self = this;
    self.rows = rows;
    self.cols = cols;
    self.table = null;
    self.data = [];
    self.results = [];
    self.maxConnections = 0;
};
BlockTable.TABLE_CLASS = 'block-table';
BlockTable.BLACK_BLOCK_CLASS = 'black';
BlockTable.prototype = {
    build: function(tagName, text) {
        var self = this,
            el = document.createElement(tagName);
        if (text !== undefined) {
            self.text(el, text);
        }
        return el;
    },
    text: function(el ,text) {
        var self = this;
        el.appendChild(document.createTextNode(text));
        return el;
    },
    setTable: function(bits) {
        var self = this,
            tr, td, bit, i, j;
        if (typeof(bits) === 'string' || bits instanceof String) {
            bits = bits.split('');
        }
        self.table = self.build('table');
        self.table.classList.add(BlockTable.TABLE_CLASS);
        self.data = [];
        for (i = 0; i < self.rows; ++i) {
            tr = self.build('tr');
            self.table.appendChild(tr);
            for (j = 0; j < self.cols; ++j) {
                bit = bits.shift() << 0;
                td = self.build('td');
                tr.appendChild(td);
                if (bit) {
                    td.classList.add(BlockTable.BLACK_BLOCK_CLASS);
                }
                self.data.push({
                    x: j,
                    y: i,
                    bit: bit,
                    el: td
                });
            }
        }
        return self;
    },
    setRandom: function() {
        var self = this,
            bits = self.randomBits(self.rows * self.cols);
        self.setTable(bits);
        return self;
    },
    randomBits: function(num) {
        var self = this,
            list = [];
        while(num--) {
            list.push(Math.floor(Math.random() * 256) % 2);
        }
        return list;
    },
    countConnections: function() {
        var self = this,
            looked = [],
            i;
        self.results = [];
        self.maxConnections = 0;
        self.data.forEach(function(block) {
            if (looked.includes(block.el)) {
                return;
            }
            var marked = [],
                count = self.countBlocks(block, marked);
            self.results.push({
                marked: marked,
                count: count
            });
            push.apply(looked, marked);
            if (count > self.maxConnections) {
                self.maxConnections = count;
            }
        });
        return self;
    },
    highlightConnections: function() {
        var self = this,
            i;
        for (i = 0; i < self.results.length; ++i) {
            if (self.results[i].count == self.maxConnections) {
                self.results[i].marked.forEach(function(el, index) {
                    self.text(el, index + 1);
                });
                break;
            }
        }
        return self;
    },
    countBlocks: function(block, marked) {
        var self = this,
            sum = 1;
        if (marked.includes(block.el)) {
            return 0;
        }
        marked.push(block.el);
        self.data.forEach(function(block_) {
            if (block.bit != block_.bit) {
                return;
            }
            if (block.x == block_.x && (block.y + 1 == block_.y || block.y - 1 == block_.y)
                || block.y == block_.y && (block.x + 1 == block_.x || block.x - 1 == block_.x)
            ) {
                sum += self.countBlocks(block_, marked);
            }
        });
        return sum;
    }
};
})(window, void 0);
